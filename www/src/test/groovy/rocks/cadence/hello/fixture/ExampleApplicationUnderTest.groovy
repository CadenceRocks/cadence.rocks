package rocks.cadence.hello.fixture

import groovy.transform.CompileStatic
import ratpack.groovy.test.GroovyRatpackMainApplicationUnderTest
import ratpack.guice.Guice
import ratpack.registry.Registry
import ratpack.remote.RemoteControl

/**
 * @author <a href="mailto:steve.good@bnsf.com">Steve Good</a>
 * @since 9/29/2015.
 */

@CompileStatic
class ExampleApplicationUnderTest extends GroovyRatpackMainApplicationUnderTest {
    protected Registry createOverrides(Registry serverRegistry) {
        Guice.registry {
            it.bindInstance RemoteControl.handlerDecorator()
        }.apply(serverRegistry)
    }
}
