package rocks.cadence.hello

import spock.lang.Specification
import spock.lang.Unroll

/**
 * @author <a href="mailto:steve.good@bnsf.com">Steve Good</a>
 * @since 9/29/2015.
 */
class HelloServiceSpec extends Specification {

    @Unroll
    void "Say hello to #name"() {
        given: 'An instance of the HelloService'
        HelloService helloService = new HelloService()

        when: 'The sayHello method is called'
        String message = helloService.sayHello(name)

        then: 'The output should be "Hello, {name}!"'
        message == "Hello, $name!"

        where: 'A list of names are provided'
        id | name
        1  | 'Steve'
        2  | 'Dave'
        3  | 'George'
    }

}
