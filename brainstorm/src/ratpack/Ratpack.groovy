import rocks.cadence.hello.HelloModule
import rocks.cadence.hello.HelloService

import static ratpack.groovy.Groovy.ratpack

ratpack {
  bindings {
    module HelloModule
  }

  handlers {
    get { HelloService helloService ->
      render helloService.helloWorld()
    }

    get(":name") { HelloService helloService ->
      render helloService.sayHello(pathTokens.name)
    }
  }
}
