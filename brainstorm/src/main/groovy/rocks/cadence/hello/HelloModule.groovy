package rocks.cadence.hello

import com.google.inject.AbstractModule
import com.google.inject.Scopes

/**
 * @author <a href="mailto:steve.good@bnsf.com">Steve Good</a>
 * @since 9/29/2015.
 */
class HelloModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(HelloService.class).in(Scopes.SINGLETON)
    }

}
