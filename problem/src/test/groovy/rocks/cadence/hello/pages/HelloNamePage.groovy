package rocks.cadence.hello.pages

/**
 * @author <a href="mailto:steve.good@bnsf.com">Steve Good</a>
 * @since 9/29/2015.
 */
class HelloNamePage extends RootPage {
    static url = '/'

    String convertToPath(String name) {
        name
    }
}
