package rocks.cadence.hello.pages

import geb.Page

/**
 * @author <a href="mailto:steve.good@bnsf.com">Steve Good</a>
 * @since 9/29/2015.
 */
class RootPage extends Page {
    static content = {
        body { $('body').text() }
    }
}
