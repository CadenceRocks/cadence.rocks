package rocks.cadence.hello

import groovy.util.logging.Slf4j

/**
 * @author <a href="mailto:steve.good@bnsf.com">Steve Good</a>
 * @since 9/29/2015.
 */

@Slf4j
class HelloService {

    String sayHello(String name) {
        log.debug "Saying hello to $name!"
        "Hello, $name!"
    }

}
