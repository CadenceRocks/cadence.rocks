import com.bnsf.user.User
import com.bnsf.user.UserModule
import com.bnsf.user.UserService
import com.zaxxer.hikari.HikariConfig
import ratpack.groovy.sql.SqlModule
import ratpack.hikari.HikariModule
import ratpack.hystrix.HystrixModule
import ratpack.rx.RxRatpack
import ratpack.server.Service
import ratpack.server.StartEvent

import static ratpack.jackson.Jackson.json
import static ratpack.groovy.Groovy.ratpack

ratpack {
  bindings {
    module HikariModule, { HikariConfig c ->
      c.setJdbcUrl('jdbc:postgresql://localhost:5432/cadence_users')
      c.setUsername('cadence_users')
      c.setPassword('cadence')
      c.setDriverClassName('org.postgresql.Driver')
    }

    module SqlModule
    module UserModule
    module new HystrixModule().sse()

    bindInstance Service, new Service() {
      @Override
      void onStart(StartEvent event) throws Exception {
        RxRatpack.initialize()
        event.registry.get(UserService).createTable()
      }
    }
  }

  handlers { UserService userService ->
    prefix('v1') {
      get {
        userService.all().toList().subscribe { List<User> users ->
          render json([users: users])
        }
      }

      get(':username') {
        userService.find(pathTokens.username).subscribe { User user ->
          render json(user)
        }
      }
    }
  }
}
