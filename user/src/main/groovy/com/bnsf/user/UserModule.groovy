package com.bnsf.user

import com.google.inject.AbstractModule
import com.google.inject.Scopes

/**
 * @author <a href="mailto:steve.good@bnsf.com">Steve Good</a>
 * @since 9/30/2015.
 */
class UserModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(UserService.class).in(Scopes.SINGLETON)
    }
}
