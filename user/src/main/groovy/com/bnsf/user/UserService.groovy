package com.bnsf.user

import com.google.inject.Inject
import groovy.util.logging.Slf4j
import rx.Observable

/**
 * @author <a href="mailto:steve.good@bnsf.com">Steve Good</a>
 * @since 9/30/2015.
 */

@Slf4j
class UserService {

    private final UserDbCommands userDbCommands

    @Inject
    UserService(UserDbCommands userDbCommands) {
        this.userDbCommands = userDbCommands
    }

    void createTable() {
        log.debug 'Creating tbl_users table if needed'
        userDbCommands.createTables()
    }

    Observable<User> all() {
        userDbCommands.getAll().map { row ->
            new User(
                username: row.username,
                email: row.email,
                displayName: row.displayName,
                active: row.active
            )
        }
    }

    Observable<User> find(String username) {
        userDbCommands.find(username).map { row ->
            new User(
                username: row.username,
                email: row.email,
                displayName: row.displayName,
                active: row.active
            )
        }
    }
}
