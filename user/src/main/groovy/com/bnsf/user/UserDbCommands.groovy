package com.bnsf.user

import com.google.inject.Inject
import com.netflix.hystrix.HystrixCommandGroupKey
import com.netflix.hystrix.HystrixCommandKey
import com.netflix.hystrix.HystrixObservableCommand
import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import ratpack.exec.Blocking

import static ratpack.rx.RxRatpack.observe
import static ratpack.rx.RxRatpack.observeEach

/**
 * @author <a href="mailto:steve.good@bnsf.com">Steve Good</a>
 * @since 9/30/2015.
 */
class UserDbCommands {

    private final Sql sql
    private static final HystrixCommandGroupKey hystrixCommandGroupKey = HystrixCommandGroupKey.Factory.asKey('sql-userdb')

    @Inject
    UserDbCommands(Sql sql) {
        this.sql = sql
    }

    void createTables() {
        sql.execute('create table if not exists tbl_users(username varchar(255) primary key,email varchar(255),displayName varchar(255),active boolean)')
    }

    rx.Observable<GroovyRowResult> getAll() {
        new HystrixObservableCommand<GroovyRowResult>(
            HystrixObservableCommand.Setter.withGroupKey(hystrixCommandGroupKey).andCommandKey(HystrixCommandKey.Factory.asKey('getAll'))
        ) {
            @Override
            protected rx.Observable<GroovyRowResult> construct() {
                observeEach(Blocking.get {
                    sql.rows("select username, email, displayName, active from tbl_users")
                })
            }

            @Override
            protected String getCacheKey() {
                "db-users-all"
            }
        }.toObservable()
    }

    rx.Observable<GroovyRowResult> find(final String username) {
        new HystrixObservableCommand<GroovyRowResult>(
                HystrixObservableCommand.Setter.withGroupKey(hystrixCommandGroupKey).andCommandKey(HystrixCommandKey.Factory.asKey('find'))
        ) {
            @Override
            protected rx.Observable<GroovyRowResult> construct() {
                observe(Blocking.get {
                    sql.firstRow("select username, email, displayName, active from tbl_users where username = $username")
                })
            }

            @Override
            protected String getCacheKey() {
                "db-users-find-$username"
            }
        }.toObservable()
    }
}
