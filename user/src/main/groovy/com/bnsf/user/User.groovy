package com.bnsf.user

import groovy.transform.Immutable

/**
 * @author <a href="mailto:steve.good@bnsf.com">Steve Good</a>
 * @since 9/30/2015.
 */

@Immutable
class User {

    String username
    String email
    String displayName
    boolean active = true

}
