package rocks.cadence.hello

import rocks.cadence.hello.fixture.ExampleApplicationUnderTest
import rocks.cadence.hello.pages.HelloNamePage
import geb.spock.GebReportingSpec
import ratpack.test.ApplicationUnderTest
import rocks.cadence.hello.pages.RootPage
import spock.lang.Shared
import spock.lang.Unroll

/**
 * @author <a href="mailto:steve.good@bnsf.com">Steve Good</a>
 * @since 9/29/2015.
 */
class HelloFunctionalSpec extends GebReportingSpec {

    @Shared
    ApplicationUnderTest aut = new ExampleApplicationUnderTest()

    def setup() {
        browser.baseUrl = aut.address.toString()
    }

    def "Say hello to the world aka, the default path"() {
        when:
        to RootPage

        then:
        body == 'Hello, World!'
    }

    @Unroll
    def "Say hello to #name"() {
        when: 'The user navigates to the name specific page'
        to HelloNamePage, name

        then: 'The body should say "Hello, {name}!"'
        body == "Hello, $name!"

        where: 'A list of names are given'
        id  |   name
        1   |   'Steve'
        2   |   'George'
        3   |   'Dave'
    }
}
